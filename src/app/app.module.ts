import { MaterialModule } from './material.module';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { LoginComponent } from './auth/login/login.component';
import { NewClientComponent } from './clients/new-client/new-client.component';
import { EditClientComponent } from './clients/edit-client/edit-client.component';
import { NewBillComponent } from './bills/new-bill/new-bill.component';
import { EditBillComponent } from './bills/edit-bill/edit-bill.component';
import { BillDetailsComponent } from './bills/bill-details/bill-details.component';
import { BillsPageComponent } from './bills/bills-page/bills-page.component';
import { ClientsPageComponent } from './clients/clients-page/clients-page.component';
import { ClientDetailsComponent } from './clients/client-details/client-details.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MyHttpInterceptor } from './interceptor/my-http-interceptor';
import { NewProvinceComponent } from './clients/new-province/new-province.component';
import { SignupComponent } from './auth/signup/signup.component';
import { NewComuneComponent } from './clients/new-comune/new-comune.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    ClientsPageComponent,    
    NewClientComponent,
    EditClientComponent,
    ClientDetailsComponent,    
    BillsPageComponent,
    NewBillComponent,
    EditBillComponent,
    BillDetailsComponent,
    NewProvinceComponent,
    SignupComponent,
    NewComuneComponent,
      
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: MyHttpInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
