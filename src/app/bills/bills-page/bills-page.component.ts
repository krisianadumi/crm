import { BillsService } from './../../services/bills.service';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';



@Component({
  selector: 'app-bills-page',
  templateUrl: './bills-page.component.html',
  styleUrls: ['./bills-page.component.css']
})
export class BillsPageComponent implements OnInit {
  displayedColumns: string[] = ['id', 'data', 'numero', 'anno', 'importo', 'stato', 'action'];
  dataSource = new MatTableDataSource();

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private BillsService: BillsService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.id !== undefined) {
        this.getBillByIdClient(params.id)
      } else {
        this.getBills()
      }

    })
  }

  getBills() {
    this.BillsService.getAllBills().subscribe(fatture => this.dataSource.data = fatture.content);
  }

  getBillByIdClient(id: number) {
    this.BillsService.getBillByClient(id).subscribe(fatture => this.dataSource.data = fatture.content)
  }

  removeBill(element:any){
    this.BillsService.deleteBill(element.id).subscribe(() => {
      alert('fattura eliminata');
      this.getBills()
  }
  )
}

  










}
