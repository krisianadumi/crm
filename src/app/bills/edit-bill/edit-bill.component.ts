import { ActivatedRoute, Router } from '@angular/router';
import { BillsService } from './../../services/bills.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-bill',
  templateUrl: './edit-bill.component.html',
  styleUrls: ['./edit-bill.component.css']
})
export class EditBillComponent implements OnInit {

  bill:any={
    data:'', 
    numero: '',
    anno: '',
    importo: '',
    stato: {
      id:'',
      nome:''
    },
    cliente:{
      id:''
    }
  }

  constructor(private BillsService:BillsService, private router:Router, private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.BillsService.getBillById(params.id).subscribe(resp => this.bill=resp))
  }

  editBill(){
    this.BillsService.updateBill(this.bill).subscribe(resp => {
    alert('Hai modificato la fattura');
    this.router.navigate(['clients'])
  })

  }

}
