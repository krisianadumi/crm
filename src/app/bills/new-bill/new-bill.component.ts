import { ClientsService } from './../../services/clients.service';
import { ActivatedRoute, Router } from '@angular/router';
import { BillsService } from './../../services/bills.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-bill',
  templateUrl: './new-bill.component.html',
  styleUrls: ['./new-bill.component.css']
})
export class NewBillComponent implements OnInit {

  bill = {
    data: '',
    numero: '',
    anno: '',
    importo: '',
    stato: {
      id: '',
      nome: ''
    },
    cliente: {
      id: ''
    }
  }

  billState!: {
    id:number,
    nome:string
  }[]

  clientName!:any

  constructor(private BillsService: BillsService, private ClientsService:ClientsService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.bill.cliente.id = params.id;
      this.getClientName(params.id)
    
    });
    this.BillsService.billState().subscribe(resp => this.billState = resp.content);
  }
  
  newBill() {
    this.BillsService.newBill(this.bill).subscribe(() => {
      alert('Fattura aggiunta')
    }
    )
  }
  
  
  getClientName(id:number){
    this.ClientsService.getClientById(id).subscribe(resp => this.clientName=resp.ragioneSociale)
  }



}
