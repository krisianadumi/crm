import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements CanActivate  {

  private token:string='';

  private login:boolean =false;

  constructor(private http:HttpClient) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.login
  }
  

  SignupNewUser(user:any){
    return this.http.post(environment.urlAPI + '/api/auth/signup', user)
  }
  

  loginUser(user:any){
    return this.http.post<any>(environment.urlAPI + '/api/auth/login', user)
  }

  setLoginTrue(){
    this.login=true;
  }

  setToken(token:string){
    this.token=token
  }

  getToken(){
    return this.token
  }

  logout(){
    return this.login=false;
  }
  
}
