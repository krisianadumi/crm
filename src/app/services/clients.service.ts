import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

type ClientiResponse = {
  content:any[]
}

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor(private http:HttpClient) { }

  getAllClients() {
    return this.http.get<ClientiResponse>(environment.urlAPI + '/api/clienti?page=0&size=150&sort=id,ASC');
  }

  deleteClient(id:number){
    return this.http.delete(environment.urlAPI + '/api/clienti/' + id)
  }

  createClient(client:any){
    return this.http.post(environment.urlAPI + '/api/clienti', client)
  }

  getClientById (id:number){
    return this.http.get<{ragioneSociale:string}>(environment.urlAPI+'/api/clienti/' +id)
  }

  updateClient(client:any){
    return this.http.put(environment.urlAPI + '/api/clienti/' + client.id, client)
  }

  getTypeOfClient(){
    return this.http.get<any>(environment.urlAPI + '/api/clienti/tipicliente')
  }

  


}


