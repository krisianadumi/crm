import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

type FattureResponse = {
  content:any[]
}

type BillStateResponse={
  content:
  {id:number, nome:string}[]
}

@Injectable({
  providedIn: 'root'
})
export class BillsService {

  constructor(private http:HttpClient) { }

  getAllBills(){    
      return this.http.get<FattureResponse>(environment.urlAPI + '/api/fatture?page=0&size=100&sort=id,ASC');    
  }

  getBillByClient(id:number){
    return this.http.get<FattureResponse>(environment.urlAPI+'/api/fatture/cliente/' +id + '?page=0&size=20&sort=id,ASC')
  }

  getBillById(id:number){
    return this.http.get(environment.urlAPI+'/api/fatture/' +id)
  }

  deleteBill(id:number){
    return this.http.delete(environment.urlAPI + '/api/fatture/' + id)
  }

  newBill(bill:any){
    return this.http.post(environment.urlAPI + '/api/fatture', bill)
  }

  updateBill(bill:any){
    return this.http.put(environment.urlAPI + '/api/fatture/' + bill.id, bill)
  }

  billState(){
    return this.http.get<BillStateResponse>(environment.urlAPI+ '/api/statifattura?page=0&size=20&sort=id,ASC')
  }

  


}
