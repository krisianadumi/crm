import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProvinceService {

  constructor(private http:HttpClient) {}

  getProvince(){
    return this.http.get<any>(environment.urlAPI+ '/api/province?page=0&size=200&sort=id,ASC')
  }

  getComuni(){
    return this.http.get<any>(environment.urlAPI + '/api/comuni?page=0&size=200&sort=id,ASC')
  }

  newProvince(province:any){
    return this.http.post(environment.urlAPI+ '/api/province', province)
  }

  newComune(comune:any){
    return this.http.post(environment.urlAPI+ '/api/comuni', comune)

  }







}
