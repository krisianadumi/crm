import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  user = {
    username: '',
    password: '',
  };

  constructor(private router: Router, private AuthService: AuthService) {}

  ngOnInit(): void {}

  loginCrm() {
    this.AuthService.loginUser(this.user).subscribe(
      (resp) => {
        this.AuthService.setLoginTrue();
        this.AuthService.setToken(resp.accessToken);

        this.router.navigate(['clients']);
      },
      (error) => alert('Non Autorizzato')
    );
  }
}
