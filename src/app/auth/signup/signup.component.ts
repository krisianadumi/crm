import { AuthService } from './../../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  newUser ={ 
    nome: '',
    cognome:'',   
    username :'',
    email:'',
    password:'',
    role:['user'],
  }

  constructor(private router:Router, private AuthService:AuthService) { }

  ngOnInit(): void {
  }

  registraNuovoUtente(){
    this.AuthService.SignupNewUser(this.newUser).subscribe(() =>{

      alert('User registrato');
      this.router.navigate(['clients']);   
    })
    
  }



}
