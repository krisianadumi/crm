import { AuthService } from './../services/auth.service';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError, tap, finalize } from 'rxjs/operators';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
    constructor(private authService:AuthService) { }
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      let ok: string;
      let beaererAuth = this.authService.getToken()
      let authReq: HttpRequest<any>  = req;
      authReq = req.clone({ headers: 
        req.headers.set("Authorization", 'Bearer ' + beaererAuth)
                  .set("X-TENANT-ID", 'fe_0321')
      });
    
     
    
    return next.handle(authReq).pipe(
      tap(
          event => {ok = event instanceof HttpResponse ? 'succeeded' : ''},
          error => { }
        ),
      catchError((error: HttpErrorResponse) => {
          return throwError(error);
       }),
      finalize(() => { })
    );
  }
}
