import { SignupComponent } from './auth/signup/signup.component';
import { LoginComponent } from './auth/login/login.component';
import { ClientDetailsComponent } from './clients/client-details/client-details.component';
import { NewBillComponent } from './bills/new-bill/new-bill.component';
import { BillsPageComponent } from './bills/bills-page/bills-page.component';
import { EditClientComponent } from './clients/edit-client/edit-client.component';
import { ClientsPageComponent } from './clients/clients-page/clients-page.component';
import { NgModule} from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewClientComponent } from './clients/new-client/new-client.component';
import { EditBillComponent } from './bills/edit-bill/edit-bill.component';
import { BillDetailsComponent } from './bills/bill-details/bill-details.component';
import { AuthService } from './services/auth.service';

const routes: Routes = [
  {path: '', redirectTo:'login', pathMatch:'full'},
  {path: 'clients', component:ClientsPageComponent, canActivate:[AuthService]},
  {path: 'client/new', component:NewClientComponent, canActivate:[AuthService]},
  {path: 'client/:id/edit', component:EditClientComponent, canActivate:[AuthService]},
  {path: 'client/:id/details', component:ClientDetailsComponent, canActivate:[AuthService]},
  {path: 'bills', component:BillsPageComponent, canActivate:[AuthService]},
  {path: 'bill/:id/new', component:NewBillComponent, canActivate:[AuthService]},
  {path: 'bill/:id/edit', component:EditBillComponent, canActivate:[AuthService]},
  {path: 'bill/:id/details', component:BillDetailsComponent, canActivate:[AuthService]},
  {path: 'login', component:LoginComponent},
  {path: 'signup', component:SignupComponent},
  {path: 'client/:id/bills', component:BillsPageComponent, canActivate:[AuthService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
