import { Router } from '@angular/router';
import { ClientsService } from './../../services/clients.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator, PageEvent } from '@angular/material/paginator';



@Component({
  selector: 'app-clients-page',
  templateUrl: './clients-page.component.html',
  styleUrls: ['./clients-page.component.css']
})
export class ClientsPageComponent implements OnInit {
  displayedColumns: string[] = ['id', 'ragioneSociale', 'partitaIva', 'tipoCliente', 'email', 'telefono', 'action'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator) paginator: MatPaginator | null = null;
  pageEvent!: PageEvent;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  constructor(private ClientsService: ClientsService, private router:Router) { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.paginator?.page.subscribe(res => console.log(res))
  }

  ngOnInit(): void {
    this.getClients()
  }
  getClients() {
    this.ClientsService.getAllClients().subscribe(clients => this.dataSource.data = clients.content);
  }

  removeClient(element: any) {
    this.ClientsService.deleteClient(element.id).subscribe(() => {
      alert('cliente cancellato');
      this.getClients()
    }
    )
  }

  selectEditClient(element:any){
    this.router.navigate(['client', element.id, 'edit'])
  }

  clientBills(element:any){
    this.router.navigate(['client', element.id, 'bills'])
  }

  newBill(element:any){
    this.router.navigate(['bill', element.id, 'new'])
  }


}
