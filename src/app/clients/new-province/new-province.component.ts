import { ProvinceService } from './../../services/province.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-new-province',
  templateUrl: './new-province.component.html',
  styleUrls: ['./new-province.component.css']
})
export class NewProvinceComponent implements OnInit {
  
  provinceToInsert={
    nome:'',
    sigla:''
  }

  constructor(private ProvinceService:ProvinceService) { }

  ngOnInit(): void {
  }

  aggiungiProvincia(){
    this.ProvinceService.newProvince(this.provinceToInsert).subscribe(resp => alert('Provincia aggiunta'))
  }


}
