import { ProvinceService } from './../../services/province.service';
import { Component, OnInit, Inject } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-new-comune',
  templateUrl: './new-comune.component.html',
  styleUrls: ['./new-comune.component.css']
})
export class NewComuneComponent implements OnInit {

  comuneToInsert={
    nome:'',
    provincia:{
      id:''
    }    
  }

  constructor(private provinceService:ProvinceService, @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.comuneToInsert.provincia.id=this.data;
  }

  aggiungiComune(){
    this.provinceService.newComune(this.comuneToInsert).subscribe(resp => alert('comune aggiunto'))
    
  }

}
