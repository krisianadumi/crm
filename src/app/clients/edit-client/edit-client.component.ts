import { ClientsService } from './../../services/clients.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {

  client:any={ragioneSociale:'', 
    partitaIva: '',
    tipoCliente: '',
    email: '',
    pec: '',
    telefono: '',
    nomeContatto: '',
    cognomeContatto: '',
    telefonoContatto: '',
    emailContatto: ''}

  constructor(private route:ActivatedRoute, private ClientsService:ClientsService, private router:Router) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.ClientsService.getClientById(params.id).subscribe(resp => this.client=resp))
  }

  editClient(){
    this.ClientsService.updateClient(this.client).subscribe(resp => {
      alert('Hai modificato il cliente');
      this.router.navigate(['clients'])
    })
  }

}
