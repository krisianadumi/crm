import { NewComuneComponent } from './../new-comune/new-comune.component';
import { NewProvinceComponent } from './../new-province/new-province.component';
import { ProvinceService } from './../../services/province.service';
import { ClientsService } from './../../services/clients.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-new-client',
  templateUrl: './new-client.component.html',
  styleUrls: ['./new-client.component.css'],
})
export class NewClientComponent implements OnInit {
  client = {
    ragioneSociale: '',
    partitaIva: '',
    tipoCliente: '',
    email: '',
    pec: '',
    telefono: '',
    nomeContatto: '',
    cognomeContatto: '',
    telefonoContatto: '',
    emailContatto: '',
    indirizzoSedeOperativa: {
      id: '',
      via: '',
      civico: '',
      cap: '',
      localita: '',
      comune: {
        id: '',
        nome: '',
        provincia: {
          id: '',
          nome: '',
          sigla: '',
        },
      },
    },
    indirizzoSedeLegale: {
      id: '',
      via: '',
      civico: '',
      cap: '',
      localita: '',
      comune: {
        id: '',
        nome: '',
        provincia: {
          id: '',
          nome: '',
          sigla: '',
        },
      },
    },
    dataInserimento: new Date(),
    dataUltimoContatto: new Date(),
  };

  typeOfClient: any = [];

  province!: any[];
  comuni!: any[];
  comuniFiltrati!: any[];
  comuniFiltratiSedeLegale!: any[];
  provinciaSelezionata!:any;

  constructor(
    private ClientsService: ClientsService,
    private router: Router,
    private ProvinceService: ProvinceService,
    public dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.ClientsService.getTypeOfClient().subscribe(
      (resp) => (this.typeOfClient = resp)
    );
    this.ProvinceService.getProvince().subscribe(
      (resp) => (this.province = resp.content)
    );
    this.ProvinceService.getComuni().subscribe(
      (resp) => (this.comuni = resp.content)
    );
  }

  createClient() {
    this.ClientsService.createClient(this.client).subscribe(() => {
      alert('Cliente aggiunto');
      this.router.navigate(['clients']);
    });
  }

  provinciaClientChanged(changed: any) {
    if (changed.value === 'aggiungiClientProvincia') {
      let dialogRef = this.dialog.open(NewProvinceComponent);

      dialogRef.afterClosed().subscribe((result) => {
        this.ProvinceService.getProvince().subscribe(
          (resp) => (this.province = resp.content)
        );
      });
    } else {
      const changedProvinceId = changed.value.id;
      this.provinciaSelezionata=changedProvinceId;
      this.comuniFiltrati = this.comuni.filter(
        (comune) => comune.provincia.id === changedProvinceId
      );
    }
  }
  comuneClientChanged(changed: any) {
    if (changed.value === 'aggiungiClientComune') {    
      let dialogRef = this.dialog.open(NewComuneComponent, {data:this.provinciaSelezionata});
      dialogRef.afterClosed().subscribe((result) => {
        this.ProvinceService.getComuni().subscribe(
          (resp) => {
            this.comuni = resp.content
            this.comuniFiltrati = this.comuni.filter(
              (comune) => comune.provincia.id === this.provinciaSelezionata
            );
          
          }
        );
      });
    } 
  }



  provinciaChangedSedeLegale(changed: any) {
    const changedProvinceId = changed.value.id;
    this.comuniFiltratiSedeLegale = this.comuni.filter(
      (comune) => comune.provincia.id === changedProvinceId
    );
  }
}
